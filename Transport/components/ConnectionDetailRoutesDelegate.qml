import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.1

import "../generalfunctions.js" as GeneralFunctions

Component {
    id: connectionDetailRoutesDelegate

    Rectangle {
        anchors {
            left: parent.left
            right: parent.right
        }
        visible: !stopPassed && connectionDetailSections.selectedIndex === 0 ? false : true
        height: visible ? stationColumn.height : 0
        color: index % 2 === 0 ? "transparent" : pageLayout.colorPalete["secondaryBG"]

        Row {
            id: stationColumn
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            spacing: units.gu(1)

            Rectangle {
                id: positionIndicator
                anchors.verticalCenter: parent.verticalCenter
                width: units.gu(1.5)
                height: width
                color: pageLayout.headerColor
                radius: width
                opacity: 0

                Timer {
                    repeat: true
                    interval: 5000
                    triggeredOnStart: true
                    running: visible
                    onTriggered: {
                        if(checkStationPassed()) {
                            positionIndicator.opacity = 1;
                        }
                        else {
                            positionIndicator.opacity = 0;
                        }
                    }

                    function checkStationPassed() {
                        var currentDate = new Date();
                        currentDate.setMinutes(currentDate.getMinutes() - delay);
                        if(arrTime) {
                            if(new Date(arrTime) <= currentDate) {
                                return true;
                            }
                            return false;
                        }
                        if(depTime) {
                            if(new Date(depTime) <= currentDate) {
                                return true;
                            }
                            return false;
                        }
                    }
                }
            }

            RowLayout {
                width: parent.width/2 - positionIndicator.width - stationColumn.spacing
                spacing: units.gu(1)

                Label {
                    id: stationNameLabel
                    Layout.preferredWidth: parent.width - units.gu(6)
                    Layout.fillWidth: true
                    
                    text: station.name || ""
                    font.pixelSize: FontUtils.sizeToPixels("normal")
                    font.bold: stopPassed ? true : false
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.Wrap
                }

                // Stop on demand
                Image {
                    Layout.preferredWidth: visible ? height : 0
                    Layout.preferredHeight: stationNameLabel.font.pixelSize
                    Layout.fillWidth: false
                    
                    source: "../icons/stop-bell.svg"
                    fillMode: Image.PreserveAspectFit
                    
                    visible: {
                        var fixedCodeEntries = fixedCodes.split(";");
                        if(fixedCodeEntries && fixedCodeEntries.length > 0) {
                            for(var i = 0; i < fixedCodeEntries.length; i++) {
                                if(fixedCodeEntries[i] === "x") {
                                    return true 
                                }
                            }
                        }
                        return false;
                    }
                }

                // Stop with a subway transfer
                Image {
                    Layout.preferredWidth: visible ? height : 0
                    Layout.preferredHeight: stationNameLabel.font.pixelSize
                    Layout.fillWidth: false
                    
                    source: "../icons/stop-metro.svg"
                    fillMode: Image.PreserveAspectFit
                    
                    visible: {
                        var fixedCodeEntries = fixedCodes.split(";");
                        if(fixedCodeEntries && fixedCodeEntries.length > 0) {
                            for(var i = 0; i < fixedCodeEntries.length; i++) {
                                if(fixedCodeEntries[i] === "-M-") {
                                    return true 
                                }
                            }
                        }
                        return false;
                    }
                }
                
                Item {
                    Layout.fillWidth: true
                }
            }

            Label {
                text: GeneralFunctions.dateToTimeString(arrTime)
                font.pixelSize: FontUtils.sizeToPixels("normal")
                font.bold: stopPassed ? true : false
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                width: parent.width/4 - parent.spacing
            }

            Label {
                text: GeneralFunctions.dateToTimeString(depTime)
                font.pixelSize: FontUtils.sizeToPixels("normal")
                font.bold: stopPassed ? true : false
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                width: parent.width/4 - parent.spacing
            }            
        }
    }
}
