import QtQuick 2.9
import QtQuick.Window 2.3
import Ubuntu.Components 1.3
import QtQuick.Controls.Suru 2.2

import "pages"
import "components"

/*!
    \brief MainView with a Label and Button elements.
*/

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "transport.zubozrout"
    
    width: units.gu(100)
    height: units.gu(75)
    
    Suru.theme: Suru.System
    
    Item {
        anchors {
            fill: parent
            bottomMargin: UbuntuApplication.inputMethod.visible ? UbuntuApplication.inputMethod.keyboardRectangle.height / Screen.devicePixelRatio : 0
        }
        
        Label {
            id: textColorText
            visible: false
        }
        
        AdaptivePageLayout {
            id: pageLayout
            anchors.fill: parent
            primaryPage: searchPage
            
            property var colorPalete: {
                "headerBG": "#00796b",
                "headerText": "#fff",
                "alternateHeaderText": "#bbb",
                "baseBG": Suru.backgroundColor,
                "baseText": textColorText.color,
                "baseAlternateText": Suru.neutralColor,
                "secondaryBG": Suru.secondaryBackgroundColor, // Qt.colorEqual(textColorText.color, "#ffffff") ? Suru.highlightColor : Suru.neutralColor,
                "secondaryText": Suru.neutralColor,
                "highlightBG": Suru.activeFocusColor,
                "highlightText": Suru.highlightColor,
                "alternateBGLight": "#006088",
                "alternateBGDark": "#004b7b",
                "warningText": "#d30",
                "successText": "#00796b"
            }

            property var headerColor: colorPalete.headerBG
            
            function setHeaderColor(color) {
                headerColor = color || colorPalete.headerBG;
            }

            layouts: [
                PageColumnsLayout {
                    when: width > units.gu(80)
                    PageColumn {
                        minimumWidth: units.gu(40)
                        maximumWidth: units.gu(50)
                        preferredWidth: units.gu(40)
                    }
                    PageColumn {
                        fillWidth: true
                    }
                },
                PageColumnsLayout {
                    when: true
                    PageColumn {
                        fillWidth: true
                        minimumWidth: units.gu(30)
                    }
                }
            ]

            SearchPage {
                id: searchPage
            }

            TransportSelectorPage {
                id: transportSelectorPage
            }

            ConnectionsPage {
                id: connectionsPage
            }

            ConnectionDetailPage {
                id: connectionDetailPage
            }

            DeparturesPage {
                id: departuresPage
            }

            AboutPage {
                id: aboutPage
            }

            SettingsPage {
                id: settingsPage
            }

            MapPage {
                id: mapPage
            }

            WelcomePage {
                id: welcomePage
            }
        }
        
        PositionSourceItem {
            id: positionSource
            active: false
        }
    }
}


