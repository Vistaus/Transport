import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.2

import "../components"

import "../transport-api.js" as Transport
import "../generalfunctions.js" as GeneralFunctions

Page {
    id: connectionDetailPage
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Connection detail")
        flickable: connectionDetailFlickable
        
        trailingActionBar {
            actions: [
                Action {
                    iconName: "share"
                    text: i18n.tr("Share")
                    onTriggered: {
                        var sharePage = pageLayout.addPageToCurrentColumn(connectionsPage, Qt.resolvedUrl("../pages/SharePage.qml"), {"string": connectionDetailPage.getTextContent()});
                    }
                },
                Action {
                    iconName: "edit-copy"
                    text: i18n.tr("Copy to clipboard")
                    onTriggered: {
                        if(connectionDetailPage.clipboard) {
                            Clipboard.push(connectionDetailPage.getTextContent());
                        }
                    }
                },
                Action {
                    iconName: "map"
                    text: i18n.tr("Map page")
                    onTriggered: {
                        mapPage.cleanPage(false);
                        pageLayout.addPageToNextColumn(connectionDetailPage, mapPage);
                        mapPage.renderRoute(connectionDetailPage.detail);
                    }
                }
            ]
            numberOfSlots: 3
        }

        extension: Sections {
            id: connectionDetailSections
            model: [i18n.tr("Only passed stations"), i18n.tr("All stations")]
            selectedIndex: 0

            StyleHints {
                sectionColor: pageLayout.colorPalete.alternateHeaderText
                selectedSectionColor: pageLayout.colorPalete.headerText
            }
        }

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.headerColor || pageLayout.colorPalete["headerBG"]
        }
    }

    clip: true

    property var detail: null
    property var clipboard: null
    
    function getTextContent() {
        var clipBoardText = connectionDetailPage.clipboard.heading;
        for(var i = 0; i < connectionDetailPage.clipboard.trains.length; i++) {
            var train = connectionDetailPage.clipboard.trains[i];
            clipBoardText += train.heading;
            for(var j = 0; j < train.route.length; j++) {
                var station = train.route[j];
                if(!station.passed && connectionDetailSections.selectedIndex === 0 ? false : true) {
                    clipBoardText += station.content;
                }
            }
        }
        return clipBoardText;
    }

    function renderDetail(detail) {
        connectionDetailModel.clearAll();
        if(detail) {
            connectionDetailPage.detail = detail;
            distanceLabel.text = detail.distance || "";
            timeLabel.text = detail.timeLength || "";
            priceLabel.text = detail.price || "";
            
            var clipboardData = {};
            clipboardData.heading = [detail.distance, detail.timeLength, detail.price].join(", ") + "\n";
            clipboardData.trains = [];
            for(var i = 0; i < detail.trainLength(); i++) {
                var train = detail.getTrain(i);
                connectionDetailModel.append(train);
                var route = detail.getTrain(i).route;
                connectionDetailModel.childModel.push(route);
                
                clipboardData.trains[i] = {};
                clipboardData.trains[i].route = [];
                clipboardData.trains[i].heading = train.trainInfo.num + " (" + train.trainInfo.typeName + ")\n";
                
                for(var j = 0; j < route.length; j++) {
                    var station = route[j];
                    var depTimeString = GeneralFunctions.dateToTimeString(station.depTime || null);
                    var arrTimeString = GeneralFunctions.dateToTimeString(station.arrTime || null);
                    clipboardData.trains[i].route[j] = {};
                    clipboardData.trains[i].route[j].content = "> " + (depTimeString ? depTimeString + " " : "* ") + (arrTimeString ? arrTimeString + " " : "") + "- " + station.station.name + "\n";
                    clipboardData.trains[i].route[j].passed = station.stopPassed;
                }
            }
            clipboard = clipboardData;
            
            connectionDetailView.forceLayout(); // Fix ListView's height
            connectionDetailColumn.forceLayout(); // Fix ColumnControl's height
        }
    }

    ConnectionDetailDelegate {
        id: connectionDetailDelegate
    }

    Flickable {
        id: connectionDetailFlickable
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: wrappingRectangle.height + wrappingRectangle.anchors.topMargin + wrappingRectangle.anchors.bottomMargin
        flickableDirection: Flickable.VerticalFlick
        
        Item {
            id: wrappingRectangle
            anchors {
                left: parent.left
                right: parent.right
                bottomMargin: units.gu(2)
            }
            height: connectionDetailColumn.spacing + dataBg.height + connectionDetailView.height
            
            Column {
                id: connectionDetailColumn
                anchors {
                    left: parent.left
                    right: parent.right
                }
                spacing: units.gu(2)
                
                Rectangle {
                    id: dataBg
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    height: dataBarRow.height + 2 * dataBarRow.anchors.margins
                    Layout.maximumWidth: parent.width - anchors.rightMargin
                    color: pageLayout.headerColor
                    
                    Rectangle {
                        anchors.fill: parent
                        color: pageLayout.colorPalete["secondaryBG"]
                    }

                    RowLayout {
                        id: dataBarRow
                        anchors {
                            left: parent.left
                            right: parent.right
                            margins: units.gu(2)
                            verticalCenter: parent.verticalCenter
                        }
                        spacing: units.gu(2)

                        ColumnLayout {
                            anchors.top: parent.top
                            Layout.fillWidth: true
                            
                            Label {
                                text: i18n.tr("Distance")
                                Layout.fillWidth: true
                                color: pageLayout.colorPalete.baseText
                                wrapMode: Text.WordWrap
                                font.bold: true
                            }

                            Label {
                                id: distanceLabel
                                text: ""
                                Layout.fillWidth: true
                                color: pageLayout.colorPalete.baseText
                                wrapMode: Text.WordWrap
                            }
                        }

                        ColumnLayout {
                            anchors.top: parent.top
                            Layout.fillWidth: true

                            Label {
                                text: i18n.tr("Time length")
                                Layout.fillWidth: true
                                color: pageLayout.colorPalete.baseText
                                wrapMode: Text.WordWrap
                                font.bold: true
                            }

                            Label {
                                id: timeLabel
                                text: ""
                                Layout.fillWidth: true
                                color: pageLayout.colorPalete.baseText
                                wrapMode: Text.WordWrap
                            }
                        }

                        ColumnLayout {
                            anchors.top: parent.top
                            Layout.fillWidth: true
                            Layout.maximumWidth: parent.width / 3

                            Label {
                                text: i18n.tr("Price")
                                Layout.fillWidth: true
                                color: pageLayout.colorPalete.baseText
                                wrapMode: Text.WordWrap
                                font.bold: true
                            }

                            Label {
                                id: priceLabel
                                text: ""
                                Layout.fillWidth: true
                                color: pageLayout.colorPalete.baseText
                                wrapMode: Text.WordWrap
                            }
                        }
                    }
                }

                ListView {
                    id: connectionDetailView
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    height: contentHeight
                    interactive: false
                    delegate: connectionDetailDelegate
                    spacing: units.gu(2)

                    model: ListModel {
                        id: connectionDetailModel
                        property var childModel: []
                        property var delayQuery: null

                        function clearAll() {
                            this.clear();
                            this.childModel = [];
                        }
                    }
                }
            }
        }
    }

    Scrollbar {
        flickableItem: connectionDetailFlickable
        align: Qt.AlignTrailing
    }
}
