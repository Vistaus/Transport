import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.1

import "../transport-api.js" as Transport
import "../generalfunctions.js" as GeneralFunctions

Item {
    id: searchItem
    width: parent.width
    height: childrenRect.height
    
    property var selectedStop: null
    property string value: textField.displayText
    property var stopData: selectedStop ? selectedStop : value
    property bool running: itemActivity.running
    property var focusCallback: null
    
    function getData() {
        var model = [];
        for(var i = 0; i < optionsModel.count; i++) {
            model.push({id: optionsModel.get(i).id, item: optionsModel.get(i).item, name: optionsModel.get(i).name});
        }
        
        return {
            model: model,
            selectedStop: selectedStop,
            value: value
        };
    }
    
    function setData(data) {
        data = data || {};
        if(data.value) {
            textField.setTextWithNoSignal(data.value);
        }
        else {
            textField.setTextWithNoSignal("");
        }
        
        if(typeof data.model !== typeof undefined && data.model.length > 0) {
            optionsModel.clear();
            for(var i = 0; i < data.model.length; i++) {
                optionsModel.append(data.model[i]);
            }
        }
        
        if(typeof data.selectedStop !== typeof undefined) {
            selectedStop = data.selectedStop;
        }
        else {
            selectedStop = null;
        }
    }
     
    function abort() {
        if(textField.cityOptions) {
            textField.cityOptions.abort();
            itemActivity.running = false;
        }
    }
    
    function empty() {
        abort();
        textField.text = "";
        selectedStop = null;
        optionsModel.clear();
    }
    
    function setFocusCallback(callback) {
        focusCallback = callback || null;
    }

    ColumnLayout {
        anchors {
            left: parent.left
            right: parent.right
        }
        spacing: 0

        TextField {
            id: textField
            anchors {
                left: parent.left
                right: parent.right
            }
            // hasClearButton: false
            placeholderText: i18n.tr("station name")

            property bool noSignal: false
            property var cityOptions: null
            property var lastTextValue: null

            onDisplayTextChanged: {
                if(lastTextValue !== textField.displayText) {
                    textChange();
                }
                lastTextValue = textField.displayText;
            }

            onFocusChanged: {
                if(!focus) {
                    optionsView.state = "hidden";
                    if(textField.cityOptions) {
                        textField.cityOptions.abort();
                    }
                }
                else {
                    optionsView.state = "visible";
                }
                
                if(searchItem.focusCallback && typeof searchItem.focusCallback === typeof function(){}) {
                    searchItem.focusCallback(focus);
                }
            }

            onAccepted: {
                if(searchItem.searchFunction) {
                    searchItem.searchFunction();
                }
            }

            function setTextWithNoSignal(text) {
                textField.noSignal = true;
                textField.text = text;
                textField.noSignal = false;
            }

            function textChange() {
                if(!textField.noSignal) {
                    var search = textField.displayText || textField.text;
                    
                    var transportOption = Transport.transportOptions.getSelectedTransport();
                    if(transportOption && search !== "") {
                        cityOptions && cityOptions.abort();
                        itemActivity.running = true;
                        optionsView.state = "visible";
                        cityOptions = transportOption.searchStations(search, function(city, data) {
                            var options = data.caller || null;
                            
                            if(data) {
                                if(data.source === "REMOTE" || search.length <= 1) {
                                    itemActivity.running = false;
                                }
                                
                                var fetchedStops = city.getInnerStops(search, true);
                                fetchedStops.sort(function(a, b) {
                                    return b.historyIndex - a.historyIndex;
                                });
                                if(fetchedStops.length > 0) {
                                    optionsModel.clear()
                                    for(var i = 0; i < fetchedStops.length; i++) {
                                        optionsModel.append({
                                            id: fetchedStops[i].getId(),
                                            item: fetchedStops[i].getItem(),
                                            name: fetchedStops[i].getName()
                                        });
                                    }
                                }
                                
                                // Erase selectedStop is current input doesn't match it.
                                if(searchItem.selectedStop && searchItem.selectedStop.getName() !== search) {
                                    searchItem.selectedStop = null;
                                }
                            }
                            else {
                                itemActivity.running = false;
                            }
                            
                            Transport.eventListener.update({
                                name: "error-message",
                                response: ""
                            });
                        }, function(data) {
                            itemActivity.running = false;
                            if(data.status !== 0) {
                                Transport.eventListener.update({
                                    name: "error-message",
                                    response: i18n.tr("Remote station search failed")
                                });
                            }
                        });
                    }
                    else {
                        optionsView.state = "hidden";
                        if(textField.cityOptions) {
                            textField.cityOptions.abort();
                            searchItem.selectedStop = null;
                            optionsModel.clear();
                            itemActivity.running = false;
                        }
                        
                        Transport.eventListener.update({
                            name: "error-message",
                            response: ""
                        });
                    }
                }
            }

            ActivityIndicator {
                id: itemActivity
                anchors {
                    fill: parent
                    centerIn: parent
                    margins: parent.height/6
                }
                running: false
            }
        }

        Component {
            id: optionsDelegate

            Rectangle {
                id: optionDelegateItemRectangle
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: stationName.contentHeight + 2 * stationName.anchors.margins
                color: pageLayout.colorPalete["secondaryBG"]
                clip: true

                Label {
                    id: stationName
                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(1)
                        centerIn: parent
                    }
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    text: name
                    font.pixelSize: FontUtils.sizeToPixels("normal")
                    wrapMode: Text.WordWrap
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(id >= 0) {
                            selectedStop = Transport.transportOptions.getSelectedTransport().cityOptions.getStopsById(id);
                            if(selectedStop) {
                                if(textField.cityOptions) {
                                    textField.cityOptions.abort();
                                    itemActivity.running = false;
                                }
                                textField.setTextWithNoSignal(selectedStop.getName());
                                textField.focus = false;
                                optionsModel.clear();
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            anchors {
                left: parent.left
                right: parent.right
            }
            height: optionsView.height
            color: pageLayout.headerColor
            clip: true

            ListView {
                id: optionsView
                anchors {
                    left: parent.left
                    right: parent.right
                }
                interactive: false
                height: childrenRect.height
                delegate: optionsDelegate

                model: ListModel {
                    id: optionsModel
                }

                state: "hidden"

                states: [
                    State {
                        name: "visible"
                        PropertyChanges { target: optionsView; visible: true }
                        PropertyChanges { target: optionsView; height: childrenRect.height }
                    },
                    State {
                        name: "hidden"
                        PropertyChanges { target: optionsView; visible: false }
                        PropertyChanges { target: optionsView; height: 0 }
                    }
                ]
            }
        }
    }
}
