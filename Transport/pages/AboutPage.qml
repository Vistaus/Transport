import QtQuick 2.9
import Ubuntu.Components 1.3
import Transport 1.0
import "../components"

import "../transport-api.js" as Transport

Page {
    id: aboutPage
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("About Transport App")
        flickable: aboutFlickable

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.colorPalete["headerBG"]
        }
    }

    clip: true

    Package {
        id: packageComp
    }

    Flickable {
        id: aboutFlickable
        anchors.fill: parent
        contentHeight: aboutFlickableRectangle.height
        flickableDirection: Flickable.VerticalFlick
        
        Item {
            id: aboutFlickableRectangle
            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(1)
            }
            height: childrenRect.height + 2 * anchors.margins

            Column {
                id: aboutColumn
                anchors {
                    left: parent.left
                    right: parent.right
                }
                spacing: units.gu(2)

                CustomDataList {
                    id: customDataList
                }

                Component.onCompleted: {
                    customDataList.append({value: i18n.tr("Transport") + " " + packageComp.version + "\nUbuntu Touch " + packageComp.framework.replace(/[^\d]+/, "") + " (" + packageComp.architecture + ")", fontScale: "large", imageSource: "../transport.svg", imageWidth: units.gu(10), bottomBorder: true});
                    customDataList.append({value: i18n.tr("Transport allows you searching mainly for Czech and Slovak public transport connections."), bottomBorder: true});
                    customDataList.append({value: i18n.tr("This application is based upon an API provided by CHAPS s.r.o. company.") + " info@chaps.cz\n\n" + i18n.tr("You can find the documentation of the API service here http://docs.crws.apiary.io/"), bottomBorder: true});
                    customDataList.append({value: i18n.tr("Feel free to report bugs on the GitLab page but note please some may be known and haven't been fixed because they are either not easy to fix or it hasn't been a priority."), fontScale: "small", bottomBorder: true});
                    customDataList.append({value: i18n.tr("Check out project's GitLab page"), url: "https://gitlab.com/zubozrout/Transport", bottomBorder: true});
                    customDataList.append({value: i18n.tr("GDPR notice: CHAPS s.r.o. API can log searched values combined with IP addresses and if such search is enabled device coordinates as well. Platform string \"ubuntu\" is passed with every single query."), fontScale: "small", bottomBorder: true});
                    customDataList.append({value: i18n.tr("Database version") + " (" + Transport.transportOptions.dbConnection.version + ")", fontScale: "small", bottomBorder: false});
                }
            }
        }
    }

    Scrollbar {
        flickableItem: aboutFlickable
        align: Qt.AlignTrailing
    }
}
