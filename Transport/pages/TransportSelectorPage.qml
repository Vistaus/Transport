import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.LocalStorage 2.0
import QtQuick.Layouts 1.2

import "../components"

import "../transport-api.js" as Transport

Page {
    id: transportSelectorPage
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Select a transport option")

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.colorPalete["headerBG"]
        }

        trailingActionBar {
            actions: [
                Action {
                    iconName: "reload"
                    text: i18n.tr("Refresh transport options")
                    onTriggered: {
                        serverUpdate();
                    }
                },
                Action {
                    iconName: transportSelectorPage.editColors ? "cancel" : "edit"
                    text: i18n.tr("Edit colors")
                    enabled: usedListModel.count > 0
                    onTriggered: {
                        transportSelectorPage.editColors = !transportSelectorPage.editColors
                    }
                }
           ]
        }
    }

    clip: true
    
    property var editColors: false
    
    onEditColorsChanged: {
        restListView.visible = false;
    }

    function serverUpdate() {
        progressLine.state = "running";
        Transport.transportOptions.fetchTrasports(true, function() {
            progressLine.state = "idle";
        });
    }

    function update() {
        progressLine.state = "running";
        Transport.transportOptions.setTransportUpdateCallback(function(options, state) {
            progressLine.state = "idle";
            
            if(!state || state === "SUCCESS") {
                if(options) {
                    usedListModel.clear();
                    restListModel.clear();
                    
                    var langCode = Transport.langCode(true);
                    for(var i = 0; i < options.transports.length; i++) {
                        var transportUsed = options.transports[i].isUsed();
                        
                        var trTypesObj = options.transports[i].getTrTypes(null, langCode);
                        var trTypes = "";
                        for(var j = 0; j < trTypesObj.length; j++) {
                            if(j > 0) {
                                trTypes += ", ";
                            }
                            trTypes += trTypesObj[j].name;
                        }

                        var item = {};
                        item.id = options.transports[i].getId();
                        item.name = options.transports[i].getName(langCode);
                        item.nameExt = options.transports[i].getNameExt(langCode);
                        item.description = options.transports[i].getDescription(langCode);
                        item.title = options.transports[i].getTitle(langCode);
                        item.city = options.transports[i].getCity(langCode);
                        item.homeState = options.transports[i].getHomeState();
                        item.ttValidFrom = options.transports[i].getTimetableInfo().ttValidFrom;
                        item.ttValidTo = options.transports[i].getTimetableInfo().ttValidTo;
                        item.isUsed = transportUsed;
                        item.trTypes = trTypes;
                        restListModel.append(item);

                        if(transportUsed && options.transports.length > 10) {
                            usedListModel.append(item);
                        }
                    }
                }
            }
            else {
                if(state !== "ABORT") {
                    errorMessage.value = i18n.tr("Failed to update transport types info");
                }
            }
        });

        Transport.transportOptions.fetchTrasports();
    }

    ProgressLine {
        id: progressLine
        anchors {
            top: pageHeader.bottom
        }
        z: 10
    }
    
    ErrorMessage {
        id: errorMessage
        anchors.top: pageHeader.bottom
    }
    
    TransportDelegate {
        id: transportDelegate
    }

    Flickable {
        id: transportSelectorFlickable
        anchors {
            top: errorMessage.bottom
            right: parent.right
            bottom: parent.bottom
            left: parent.left
        }
        contentWidth: parent.width
        contentHeight: transportSelectorColumn.height
        flickableDirection: Flickable.VerticalFlick

        Column {
            id: transportSelectorColumn
            anchors {
                left: parent.left
                right: parent.right
            }
            height: childrenRect.height

            ListItem {
                visible: usedListModel.count > 0 && usedListModel.count !== restListModel.count

                Rectangle {
                    anchors {
                        margins: units.gu(2)
                        left: parent.left
                        right: parent.right
                    }
                    height: parent.height
                    color: "transparent"

                    Label {
                        text: i18n.tr("Used transport types") + " (" + usedListModel.count + ")"
                        width: parent.width
                        font.italic: true
                        wrapMode: Text.WordWrap
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }

            ListView {
                id: usedListView
                width: parent.width
                height: childrenRect.height
                interactive: false
                model: ListModel {
                    id: usedListModel
                }
                delegate: transportDelegate
            }

            ListItem {
                visible: !transportSelectorPage.editColors
                Rectangle {
                    anchors {
                        margins: units.gu(2)
                        left: parent.left
                        right: parent.right
                    }
                    height: parent.height
                    color: "transparent"

                    Label {
                        text: i18n.tr("All transport types") + " (" + restListModel.count + ")"
                        width: parent.width
                        font.italic: true
                        wrapMode: Text.WordWrap
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    ActivityIndicator {
                        anchors.centerIn: parent
                        running: progressLine.state === "running"
                        z: 1
                    }

                    Button {
                        width: units.gu(4)
                        anchors {
                            right: parent.right
                            verticalCenter: parent.verticalCenter
                        }
                        visible: usedListModel.count > 0
                        color: "transparent"

                        onClicked: {
                            restListView.visible = !restListView.visible
                        }

                        Icon {
                            anchors {
                                fill: parent
                                margins: units.gu(0.75)
                            }
                            name: restListView.visible ? "close" : "add"
                            color: pageLayout.colorPalete.baseText
                        }
                    }
                }
            }

            ListView {
                id: restListView
                width: parent.width
                height: visible ? childrenRect.height : 0
                interactive: false
                model: ListModel {
                    id: restListModel
                }
                delegate: transportDelegate

                Component.onCompleted: {
                    Transport.transportOptions.dbConnection.onLoad(function() {
                        transportSelectorPage.update();
                        searchPage.ignoreGps = false;
                        visible = usedListModel.count === 0;
                    });
                }
            }
        }
    }

    Scrollbar {
        flickableItem: transportSelectorFlickable
        align: Qt.AlignTrailing
    }
}
