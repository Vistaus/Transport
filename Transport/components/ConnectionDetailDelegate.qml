import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.1

import "../generalfunctions.js" as GeneralFunctions
import "../transport-api.js" as Transport

Component {
    id: connectionDetailDelegate

    Item {
        anchors {
            left: parent.left
            right: parent.right
        }
        height: 2 * routeColumn.spacing + transportRow.height + routeHeader.height + connectionDetailRoutesView.contentHeight

        ConnectionDetailRoutesDelegate {
            id: connectionDetailRoutesDelegate
        }

        ColumnLayout {
            id: routeColumn
            anchors {
                left: parent.left
                right: parent.right
            }
            
            Item {
                id: transporRowWrapper
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }
                height: transportRow.childrenRect.height
                
                RowLayout {
                    id: transportRow
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    spacing: units.gu(1)
                    
                    TransportIcon {
                        id: transportTypeIconItem
                        Layout.preferredWidth: units.gu(3)
                        Layout.maximumWidth: units.gu(3)
                        Component.onCompleted: {
                            setData(trainInfo.id, trainInfo.num);
                        }
                    }
                    
                    Label {
                        text: trainInfo.typeName ? trainInfo.typeName.charAt(0).toUpperCase() + trainInfo.typeName.slice(1) : ""
                        font.pixelSize: FontUtils.sizeToPixels("medium")
                        font.bold: false
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap

                        Layout.fillWidth: false
                        Layout.fillHeight: true
                    }

                    Label {
                        text: trainInfo.num || ""
                        font.pixelSize: FontUtils.sizeToPixels("large")
                        font.bold: true
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                    
                    Label {
                        id: delayLabel
                        text: delay ? i18n.tr("%1 minute delay", "%1 minutes delay", delay).arg(delay) : ""
                        font.pixelSize: FontUtils.sizeToPixels("medium")
                        font.bold: true
                        color: upToDate ? pageLayout.colorPalete["warningText"] : pageLayout.colorPalete["secondaryText"]
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        
                        property bool upToDate: true

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                }
            }

            Row {
                id: routeHeader
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }
                spacing: units.gu(1)

                Label {
                    text: i18n.tr("Station name")
                    width: parent.width/2
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                }

                Label {
                    text: i18n.tr("Arrival")
                    width: parent.width / 4 - parent.spacing
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                }

                Label {
                    text: i18n.tr("Departure")
                    width: parent.width / 4 - parent.spacing
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                }
            }

            ListView {
                id: connectionDetailRoutesView
                anchors {
                    left: parent.left
                    right: parent.right
                }
                implicitHeight: contentHeight
                interactive: false
                delegate: connectionDetailRoutesDelegate

                model: ListModel {
                    id: connectionDetailRoutesModel
                }

                Component.onCompleted: {
                    var stops = connectionDetailModel.childModel[index];
                    if(stops) {
                        for(var i = 0; i < stops.length; i++) {
                            connectionDetailRoutesModel.append(stops[i]);
                        }
                    }
                }
            }
        }
        
        DelayTimer {
            id: delayTimer
            enabled: true
            conId: connectionsID
            query: typeof delayQuery !== typeof undefined ? delayQuery : null
            callback: function(response) {
                response = response || {};
                if(typeof response.delay !== typeof undefined) {
                    delay = response.delay;
                    delayLabel.upToDate = true;
                }
                else {
                    delayLabel.upToDate = false;
                }
            }
        }
    }
}
