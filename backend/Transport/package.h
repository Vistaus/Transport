#ifndef PACKAGE_H
#define PACKAGE_H

#include <QObject>

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QTextStream>

class Package : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString version READ version )
    Q_PROPERTY(QString name READ name )
    Q_PROPERTY(QString description READ description )
    Q_PROPERTY(QString title READ title )
    Q_PROPERTY(QString maintainer READ maintainer )
    Q_PROPERTY(QString framework READ framework )
    Q_PROPERTY(QString architecture READ architecture )

public:
    explicit Package(QObject *parent = 0);
    ~Package();

public slots:

Q_SIGNALS:

protected:
    QString version() {
        return appVersion;
    }
    
    QString name() {
        return appName;
    }
    
    QString description() {
        return appDescription;
    }
    
    QString title() {
        return appTitle;
    }
    
    QString maintainer() {
        return appMaintainer;
    }
    
    QString framework() {
        return appFramework;
    }
    
    QString architecture() {
        return appArchitecture;
    }

private:
    QJsonObject getManifest();
    
    QString appVersion;
    QString appName;
    QString appDescription;
    QString appTitle;
    QString appMaintainer;
    QString appFramework;
    QString appArchitecture;
};

#endif // PACKAGE_H

