"use strict";

function langCode(toInt) {
    var locale = Qt.locale().name;
    var lang = "";
    var num = 0;

    switch(locale.substring(0,2)) {
        case "en":
            lang = "ENGLISH";
            num = 1;
            break;
        case "de":
            lang = "GERMAN";
            num = 2;
            break;
        case "cs":
            lang = "CZECH";
            num = 0;
            break;
        case "sk":
            lang = "SLOVAK";
            num = 3;
            break;
        case "pl":
            lang = "POLISH";
            num = 4;
            break;
        default:
            lang = "ENGLISH";
            num = 1;
            break;
    }

    if(toInt !== undefined && toInt === true) {
        return num;
    }
    return lang;
}

function getTranpsortType(index) {
    switch(index) {
    case 1:
       return "train";
    case 2:
        return "bus";
    case 3:
        return "tram";
    case 4:
       return "trol";
    case 5:
       return "metro";
    case 6:
       return "ship";
    case 7:
       return "air";
    case 8:
       return "taxi";
    case 9:
       return "cableway";
    default:
       return "empty";
    }
}

function dateStringtoDate(dateString) {
    if(!dateString) {
        return "";
    }

    var parts = dateString.split(" ");
    var date = parts[0];
    var time = parts[1];

    var dateTime = {
        day: 0,
        month: 0,
        year: 0,
        hours: 0,
        minutes: 0
    }

    if(date) {
        var dateParts = date.split(".");
        dateTime.day = dateParts[0];
        dateTime.month = parseInt(dateParts[1]) - 1;
        dateTime.year = dateParts[2];
    }

    if(time) {
        var timeParts = time.split(":");
        dateTime.hours = timeParts[0];
        dateTime.minutes = timeParts[1];
    }

    var finalDate = new Date(dateTime.year, dateTime.month, dateTime.day, dateTime.hours, dateTime.minutes, 0, 0, {timeZone:"Europe/Prague"});

    return finalDate;
}

function dateToTimeString(date) {
    if(date) {
        date = new Date(date);

        var hours = String(date.getHours());
        var minutes = String(date.getMinutes());
        if(hours.length === 1) {
            hours = "0" + hours;
        }
        if(minutes.length === 1) {
            minutes = "0" + minutes;
        }
        return hours + ":" + minutes;
    }
    return "";
}

function dateToString(date) {
    if(date) {
        date = new Date(date);

        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        return day + "." + month + "." + year + " " + dateToTimeString(date);
    }
    return "";
}

function lineColor(typeId, line) {
    switch(typeId) {
        case 5: //subway
            switch(line.toLowerCase()) {
                case "a":
                    return "#1ba300";
                case "b":
                    return "#ffd432";
                case "c":
                    return "#ff2730";
                case "d":
                    return "#1A237E";
            }
            break;
        case 1: // train
            return "#373d68";
        case 3: // tram
            return "#7a0603";
        case 4: // trolleybus
            return "#9f367a";
        case 6: // ship
            return "#00b3cb";
        case 9: // cableway
            return "#bad147";
        default:
            return "#000000";
    }
}

function setStopData(stopSearch, stopidfrom, stopnamefrom, typeid) {
    stopSearch.setData({
        selectedStop: new (typeof Transport !== typeof undefined ? Transport.Stop : Stop)({
            id: stopidfrom,
            item: {
                name: stopnamefrom
            }
        }, {
            transportID: typeid,
            dbConnection: (typeof Transport !== typeof undefined ? Transport.transportOptions.dbConnection : transportOptions.dbConnection)
        }),
        value: stopnamefrom
    });
}

// SOURCE: http://stackoverflow.com/a/21623206/1642887
function latLongDistance(lat1, lon1, lat2, lon2) {
  var p = Math.PI / 180;
  var c = Math.cos;
  var a = 0.5 - c((lat2 - lat1) * p)/2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p))/2;

  return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

function remainingTimeLabel(toDate) {
    var currentDate = new Date();

    if(currentDate - 60000 <= toDate) {
        var delta = (toDate - currentDate) / 1000;
        var days = Math.floor(delta / 3600 / 24);
        var hours = Math.floor(delta / 3600) % 24;
        var minutes = Math.floor(delta / 60) % 60;
        var seconds = Math.floor(delta) % 60;

        if(days <= 0) {
            if(hours < 1) {
                if(minutes < 0) {
                    return {
                        text: i18n.tr("now"),
                        inFuture: true
                    }
                }
                else {
                    minutes++;
                    return {
                        text: i18n.tr("in %1 minute", "in %1 minutes", minutes).arg(minutes),
                        inFuture: true
                    }
                }
            }
            else {
                if(hours < 10) {
                    hours = "0" + hours;
                }

                if(minutes < 10) {
                    minutes = "0" + minutes;
                }

                if(seconds < 10) {
                    seconds = "0" + seconds;
                }

                return {
                    text: hours + ":" + minutes + ":" + seconds,
                    inFuture: true
                }
            }
        }
        else {
            return {
                text: i18n.tr("in %1 day", "in %1 days", days).arg(days),
                inFuture: true
            }
        }
    }
    else {
        return {
            text: i18n.tr("departed"),
            inFuture: false
        }
    }
}
