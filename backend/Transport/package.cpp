#include "package.h"

Package::Package(QObject *parent) : QObject(parent) {
    QJsonObject manifestObject = getManifest();
   
    appVersion = manifestObject.value("version").toString();
    appName = manifestObject.value("name").toString();
    appDescription = manifestObject.value("description").toString();
    appTitle = manifestObject.value("title").toString();
    appMaintainer = manifestObject.value("maintainer").toString();
    appFramework = manifestObject.value("framework").toString();
    appArchitecture = manifestObject.value("architecture").toString();
}

Package::~Package() {
}

QJsonObject Package::getManifest() {
    QFile file("Transport/manifest.json");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream(stdout) << "Could not load manifest json file." << endl;
        return QJsonObject();
    }
    
    QString fileContent = file.readAll();
    file.close();
    
    QJsonDocument document = QJsonDocument::fromJson(fileContent.toUtf8());
    return document.object();
}
