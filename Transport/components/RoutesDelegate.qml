import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import "../generalfunctions.js" as GeneralFunctions
import "../transport-api.js" as Transport

Component {
    id: routesDelegate

    Item {
        anchors {
            left: parent.left
            right: parent.right
            topMargin: units.gu(1)
            bottomMargin: units.gu(1)
        }
        height: childrenRect.height

        RowLayout {
            id: routeRow
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: units.gu(2)

            ColumnLayout {
                Layout.fillWidth: false
                Layout.fillHeight: true
                Layout.minimumWidth: units.gu(4)
                Layout.preferredWidth: units.gu(4)
                spacing: units.gu(0.05)
                
                TransportIcon {
                    id: transportTypeIconItem
                    Component.onCompleted: {
                        setData(typeIndex, num);
                    }
                }

                Label {
                    text: num
                    fontSizeMode: Text.Fit
                    font.pixelSize: FontUtils.sizeToPixels("large")
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap

                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignCenter
                }
            }

            GridLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                columns: 2

                Label {
                    text: from.name || ""
                    font.pixelSize: FontUtils.sizeToPixels("normal")
                    font.bold: false
                    wrapMode: Text.WordWrap

                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                }

                RowLayout {
                    spacing: units.gu(1)
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.minimumWidth: delay ? routeRow.width / 2 : 0;

                    Label {
                        id: delayLabel
                        text: delay ? i18n.tr("%1 minute delay", "%1 minutes delay", delay).arg(delay) : ""
                        font.pixelSize: FontUtils.sizeToPixels("normal")
                        font.bold: true
                        color: upToDate ? pageLayout.colorPalete["warningText"] : pageLayout.colorPalete["secondaryText"]
                        horizontalAlignment: Text.AlignRight
                        wrapMode: Text.WordWrap
                        visible: text !== ""
                        
                        property bool upToDate: true

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignLeft
                    }

                    Label {
                        text: from.time || ""
                        font.pixelSize: FontUtils.sizeToPixels("normal")
                        font.bold: false
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignRight

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignLeft
                    }
                }

                Label {
                    text: to.name || ""
                    font.pixelSize: FontUtils.sizeToPixels("normal")
                    font.bold: false
                    wrapMode: Text.WordWrap

                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                }

                Label {
                    text: to.time || ""
                    font.pixelSize: FontUtils.sizeToPixels("normal")
                    font.bold: false
                    horizontalAlignment: Text.AlignRight
                    wrapMode: Text.WordWrap

                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignRight
                }
            }
        }
        
        DelayTimer {
            id: delayTimer
            enabled: true
            conId: connectionsID
            query: typeof delayQuery !== typeof undefined ? delayQuery : null
            callback: function(response) {
                response = response || {};
                if(typeof response.delay !== typeof undefined) {
                    delay = response.delay;
                    delayLabel.upToDate = true;
                }
                else {
                    delayLabel.upToDate = false;
                }
            }
        }
    }
}
