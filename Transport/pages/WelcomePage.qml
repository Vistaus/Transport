import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

import "../transport-api.js" as Transport

import "../components"

Page {
    id: welcomePage
    clip: true
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Welcome to Transport")
        flickable: welcomeFlickable

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.colorPalete["headerBG"]
        }
    }
        
    Flickable {
        id: welcomeFlickable
        anchors.fill: parent
        contentHeight: welcomeFlickableRectangle.height
        flickableDirection: Flickable.VerticalFlick
        
        Item {
            id: welcomeFlickableRectangle
            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(1)
            }
            height: welcomeColumn.height + 2 * anchors.margins
            
            Column {
                id: welcomeColumn
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }
                
                CustomDataList {
                    id: customDataList
                    
                    Component.onCompleted: {
                        customDataList.append({
                            value: i18n.tr("Welcome to Transport"),
                            fontScale: "large",
                            imageSource: "../transport.svg",
                            imageWidth: units.gu(10),
                            bottomBorder: true
                        });
                        
                        customDataList.append({
                            value: i18n.tr("Search for mainly Czech and Slovak public transport connections with ease."),
                            bottomBorder: false
                        });
                        
                        customDataList.append({
                            value: i18n.tr("Start by selecting transport option on the main search page."),
                            bottomBorder: true
                        });
                        
                        customDataList.append({
                            value: i18n.tr("Not sure which direction to go? Try searching all departures from a respective station."),
                            imageSource: "../transport-blue.svg",
                            imageWidth: units.gu(4),
                            bottomBorder: true
                        });
                        
                        customDataList.setCallbackFor(customDataList.count(), function() {
                            pageLayout.addPageToNextColumn(searchPage, settingsPage);
                            Transport.transportOptions.saveDBSetting("not-first-run", 1);
                        }, false);
                        
                        customDataList.append({
                            value: i18n.tr("Don't like the defaults? Customize app settings more to your liking."),
                            buttonIcon: "settings",
                            bottomBorder: false
                        });
                    }
                }
                
                RowPicker {
                    id: welcomePicker
                    
                    property var render: function(model) {
                        clear();
                        
                        var options = [i18n.tr("Get me to the app")];
                        var index = 0;
                        
                        initialize(options, index, function(itemIndex) {
                            pageLayout.removePages(welcomePage);
                            Transport.transportOptions.saveDBSetting("not-first-run", 1);
                        });
                    }

                    Component.onCompleted: {
                        welcomePicker.update(function(model) { welcomePicker.render(model) });
                    }
                }
            }
            
        }
    }

    Scrollbar {
        flickableItem: welcomeFlickable
        align: Qt.AlignTrailing
    }
}
