import QtQuick 2.9
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3
import QtQuick.Controls.Suru 2.2

import "../components"

import "../transport-api.js" as Transport
import "../generalfunctions.js" as GeneralFunctions

Page {
    id: searchPage
    clip: true
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Transport")

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.headerColor || pageLayout.colorPalete["headerBG"]
        }

        leadingActionBar {
            id: leadActions
            actions: [
                Action {
                    text: i18n.tr("Settings")
                    iconName: "navigation-menu"
                    onTriggered: {
                        mainMenu.toggle();
                    }
                }
            ]
            numberOfSlots: 1
        }

        trailingActionBar {
            actions: [
                Action {
                    id: headerTrailingSearchResultsIcon
                    iconName: "search"
                    text: i18n.tr("Search results")
                    onTriggered: {
                        pageLayout.addPageToNextColumn(searchPage, connectionsPage);
                    }
                    enabled: false
                    visible: enabled
                }
           ]
           numberOfSlots: 1
        }
        
        extension: Sections {
            id: searchSections
            model: [i18n.tr("Connections"), i18n.tr("Station departures")]
            selectedIndex: 0
            
            onSelectedIndexChanged: {
                rowSwitchLayout.currentIndex = selectedIndex;
            }

            StyleHints {
                sectionColor: pageLayout.colorPalete.alternateHeaderText
                selectedSectionColor: pageLayout.colorPalete.headerText
            }
        }
    }
    
    property var ignoreGps: false
    
    property var searchConnectionsContainerLink: searchConnectionsContainer;
    property var rowSwitchLayoutLink: rowSwitchLayout;
    
    function displaySearchResultsIcons(show) {
        if(show) {
            headerLeadingSearchResultsIcon.enabled = true;
            headerTrailingSearchResultsIcon.enabled = true;
        }
        else {
            headerLeadingSearchResultsIcon.enabled = false;
            headerTrailingSearchResultsIcon.enabled = false;
        }
    }
    
    Rectangle {
        anchors {
            top: parent.top
            left: parent.left
            leftMargin: units.gu(1)
            topMargin: leadActions.height
        }
        z: 10
        width: mainMenu.width
        height: mainMenu.height
        color: pageLayout.colorPalete["baseBG"]
        visible: mainMenu.shouldBeVisible
        
        Menu {
            id: mainMenu
            
            MenuItem {
                id: headerLeadingSearchResultsIcon
                icon: "search"
                text: i18n.tr("Search results")
                onTriggered: pageLayout.addPageToNextColumn(searchPage, connectionsPage)
                enabled: false
            }
            MenuItem {
                icon: "search" // icon: Qt.resolvedUrl("../images/icon-route.svg")
                text: i18n.tr("Nearby route")
                // enabled: transportOptionLabel.ok
                onTriggered: {
                    searchPage.ignoreGps = false;
                    positionSource.append(function(source) {
                        if(source.isValid) {
                            var matchFound = searchConnectionsContainer.findClosestRouteInHistory({
                                callback: function(found) {
                                    if(!found) {
                                        errorMessage.value = i18n.tr("No cached nearby routes found");
                                    }
                                }
                            });
                        }
                        else {
                            errorMessage.value = i18n.tr("Location search disabled or not functional");
                        }
                    });
                    rowSwitchLayout.currentIndex = 0;
                }
            }
            MenuItem {
                icon: "search" // icon: Qt.resolvedUrl("../images/icon-stop.svg")
                text: i18n.tr("Nearby station")
                // enabled: transportOptionLabel.ok
                onTriggered: {
                    searchPage.ignoreGps = false;
                    positionSource.append(function(source) {
                        if(source.isValid) {
                            var matchFound = searchConnectionsContainer.findClosestRouteInHistory({
                                justStop: true,
                                callback: function(found) {
                                    if(!found) {
                                        errorMessage.value = i18n.tr("No nearby stations found");
                                    }
                                }
                            });
                        }
                        else {
                            errorMessage.value = i18n.tr("Location search disabled or not functional");
                        }
                    });
                    rowSwitchLayout.currentIndex = 0;
                }
            }
            MenuItem {
                icon: "map"
                text: i18n.tr("Map page")
                onTriggered: {
                    mapPage.cleanPage(true);
                    pageLayout.addPageToNextColumn(searchPage, mapPage);
                    mapPage.renderAllDBStations(Transport.transportOptions.getSelectedId());
                }
            }
            MenuItem {
                icon: "settings"
                text: i18n.tr("Settings")
                onTriggered: {
                    pageLayout.addPageToNextColumn(searchPage, settingsPage);
                }
            }
            MenuItem {
                icon: "help"
                text: i18n.tr("About")
                onTriggered: {
                    pageLayout.addPageToNextColumn(searchPage, aboutPage);
                }
            }
        }
    }
    
    Item {
        id: pageContent
        anchors {
            top: pageHeader.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
    
        MouseArea {
            anchors.fill: parent
            onClicked: {
                parent.focus = true;
            }
            
            property var startX: 0
            onPressed: {
                startX = mouseX;
            }
            
            onReleased: {
                var diff = startX - mouseX;
                if(Math.abs(diff) > width / 4) {
                    if(diff > 0) {
                        rowSwitchLayout.currentIndex = 1;
                    }
                    else {
                        rowSwitchLayout.currentIndex = 0;
                    }
                }
            }
        }
        
        Item {
            id: searchContainer
            anchors.fill: parent
            
            ProgressLine {
                id: progressLine
                z: 10
                
                Component.onCompleted: {
                    Transport.eventListener.listen({
                        name: "fetching-connections",
                        callback: function(value) {
                            if(value) {
                                state = "running";
                            }
                            else {
                                state = "idle";
                            }
                        }
                    });
                    
                    Transport.eventListener.listen({
                        name: "fetching-departures",
                        callback: function(value) {
                            if(value) {
                                state = "running";
                            }
                            else {
                                state = "idle";
                            }
                        }
                    });
                    
                    Transport.eventListener.listen({
                        name: "geo-search",
                        callback: function(value) {
                            if(value) {
                                state = "running";
                            }
                            else {
                                state = "idle";
                            }
                        }
                    });
                }
            }
            
            ErrorMessage {
                id: errorMessage
                
                Component.onCompleted: {
                    Transport.eventListener.listen({
                        name: "fetching-connections-error",
                        callback: function(value) {
                            if(value === "fail") {
                                errorMessage.value = i18n.tr("Search failed");
                            }
                        }
                    });
                    
                    Transport.eventListener.listen({
                        name: "fetching-departures-error",
                        callback: function(value) {
                            if(value === "fail") {
                                errorMessage.value = i18n.tr("Search failed");
                            }
                        }
                    });
                }
            }

            Column {
                id: pageColumn
                anchors {
                    top: errorMessage.bottom
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                spacing: units.gu(1)
                
                onWidthChanged: {
                    rowSwitchLayout.updatePosition();
                }
                
                TransportOptionWindow {
                    id: transportOptionWindow
                }
                
                RowSwitchLayout {
                    id: rowSwitchLayout
                    count: 2
                    currentIndex: 0
                    height: pageColumn.height - pageColumn.spacing - transportOptionWindow.height - units.gu(4) // Exclude bottom kick
                    
                    Component.onCompleted: {
                        currentIndex = Number(Transport.transportOptions.getDBSetting("last-search-sections-index") || 0);
                    }
                    
                    onCurrentIndexChanged: {
                        searchSections.selectedIndex = currentIndex;
                        Transport.transportOptions.saveDBSetting("last-search-sections-index", currentIndex);
                    }
                    
                    SearchConnections {
                        id: searchConnectionsContainer
                        Layout.alignment: Qt.AlignTop
                        Layout.preferredWidth: pageColumn.width
                        Layout.fillHeight: true
                    }
                    
                    SearchDepartures {
                        id: searchDeparturesContainer
                        Layout.alignment: Qt.AlignTop
                        Layout.preferredWidth: pageColumn.width
                        Layout.fillHeight: true
                    }
                }
            }
        }
    }
    
    RecentBottomEdge {
        id: bottomEdge
    }
}
