import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.2
import Ubuntu.Components.Popups 1.3

import "../components"

import "../transport-api.js" as Transport
import "../generalfunctions.js" as GeneralFunctions

Page {
    id: connectionsPage
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Connections")

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.headerColor || pageLayout.colorPalete["headerBG"]
        }

        trailingActionBar {
            actions: [
                Action {
                    id: trailingdelete
                    iconName: "delete"
                    text: i18n.tr("Delete from list")
                    visible: false
                    onTriggered: {
                        if(connections) {
                            var selectedTransport = Transport.transportOptions.getSelectedTransport();
                            var currentConnection = selectedTransport.getConnectionById(connections.listId);
                            if(currentConnection) {
                                var currentConnectionId = currentConnection.listId;
                                var currentConnectionIndex = selectedTransport.getConnectionIndexById(currentConnectionId);
                                var allConnections = selectedTransport.removeConnectionById(currentConnectionId);
                                if(allConnections.length > 0) {
                                    var selectedConnection = allConnections[0];
                                    connections = selectedConnection;
                                    refreshPage(true);
                                }
                                else {
                                    searchPage.displaySearchResultsIcons(false);
                                    connections = null;
                                    connectionsModel.clearAll();
                                    pageLayout.removePages(connectionsPage);
                                }
                            }
                            else {
                                console.error("Mismatch in TransportOptions instance not containing currently displayed connection.");
                            }
                        }
                    }
                },
                Action {
                    id: trailinginfo
                    iconName: "info"
                    text: i18n.tr("Info")
                    visible: false
                    onTriggered: {
                        PopupUtils.open(infoBox);
                    }
                },
                Action {
                    id: trailingdepartures
                    iconName: "search"
                    text: i18n.tr("Station departures")
                    visible: false
                    onTriggered: {
                        var transport = Transport.transportOptions.getSelectedTransport();
                        if(transport) {
                            var allConnections = transport.getAllConnections();
                            if(allConnections.length > 0) {
                                var currentConnectionIndex = allConnections.indexOf(connections);
                                var currentConnection = allConnections[currentConnectionIndex];
                                
                                progressLine.state = "running";
                                transport.departures({
                                    stop: currentConnection.from.getName(),
                                    isDep: true
                                }, function(departures) {
                                    progressLine.state = "idle";
                                    if(departures.statusOk) {
                                        departuresPage.loadList({
                                            departures: departures
                                        });
                                        pageLayout.addPageToNextColumn(connectionsPage, departuresPage);
                                    }
                                    else {
                                        errorMessage.value = i18n.tr("No results available for the selected search. Try inserting the exact name of the station rather than only a city or location name.");
                                    }
                                });
                            }
                        }
                    }
                },
                Action {
                    id: trailingNext
                    iconName: "next"
                    text: i18n.tr("Next")
                    visible: false
                    onTriggered: {
                        var allConnections = Transport.transportOptions.getSelectedTransport().getAllConnections();
                        var currentConnectionIndex = allConnections.indexOf(connections);
                        var newConnectionIndex = currentConnectionIndex < allConnections.length - 1 ? currentConnectionIndex + 1 : 0;
                        var selectedConnection = allConnections[newConnectionIndex];
                        connections = selectedConnection;
                        renderAllConnections(selectedConnection);
                        scrollTo(0);

                        console.log(currentConnectionIndex, newConnectionIndex, "all:", allConnections.length);
                    }
                },
                Action {
                    id: trailingPrevious
                    iconName: "previous"
                    text: i18n.tr("Previous")
                    visible: false
                    onTriggered: {
                        var allConnections = Transport.transportOptions.getSelectedTransport().getAllConnections();
                        var currentConnectionIndex = allConnections.indexOf(connections);
                        var newConnectionIndex = currentConnectionIndex > 0 ? currentConnectionIndex - 1 : allConnections.length - 1;
                        var selectedConnection = allConnections[newConnectionIndex];
                        connections = selectedConnection;
                        renderAllConnections(selectedConnection);
                        scrollTo(0);

                        console.log(currentConnectionIndex, newConnectionIndex, "all:", allConnections.length);
                    }
                }
            ]
            numberOfSlots: 5
        }
    }

    clip: true

    property var connections: null
    property var transport: null
    property string overviewText: ""

    onVisibleChanged: {
        if(visible) {
            refreshPage();
        }
    }
    
    function refreshPage(forceRefresh) {
        var currentTransport = Transport.transportOptions.getSelectedTransport();
        if(currentTransport) {
            if(transport !== null && (forceRefresh || transport !== currentTransport)) {
                var allConnections = currentTransport.getAllConnections();
                if(allConnections.length > 0) {
                    renderAllConnections(allConnections[0]);
                }
                else {
                    connectionsModel.clearAll();
                }
                enableHeaderButtons(allConnections);
            }
            transport = currentTransport;
        }
    }

    function updateConnectionDetail() {
        var transport = Transport.transportOptions.getSelectedTransport();
        if(transport) {
            var allConnections = transport.getAllConnections();
            if(allConnections.length > 0) {
                var currentConnectionIndex = allConnections.indexOf(connections);
                var currentConnection = allConnections[currentConnectionIndex];

                if(currentConnection) {
                    var from = "";
                    if(currentConnection.from) {
                        from = (currentConnection.from.getName());
                    }
                    var to = "";
                    if(currentConnection.to) {
                        to = (currentConnection.to.getName());
                    }
                    var via = "";
                    if(currentConnection.via) {
                        via = (currentConnection.via.getName());
                    }

                    var connectionsLength = currentConnection.connections.length;

                    var finalText = i18n.tr("Journey start: %1", from).arg(from) + "\n";
                    finalText += i18n.tr("Journey end: %1", to).arg(to) + "\n";
                    finalText += (via ? i18n.tr("Transfering at: %1", via).arg(via) + "\n" : "");
                    finalText += i18n.tr("Number of results: %1", connectionsLength).arg(connectionsLength) + "\n";
                    this.overviewText = finalText;

                    return true;
                }
            }
        }
        return false;
    }

    function enableHeaderButtons(connections) {
        if(connections.length > 1) {
            trailingNext.visible = true;
            trailingPrevious.visible = true;
        }
        else {
            trailingNext.visible = false;
            trailingPrevious.visible = false;
        }

        if(connections.length > 0) {
            trailingdelete.visible = true;
            trailinginfo.visible = true;
            trailingdepartures.visible = true;
        }
        else {
            trailingdelete.visible = false;
            trailinginfo.visible = false;
            trailingdepartures.visible = false;
        }
    }

    function appendConnections() {
        if(connectionsPage.connections) {
            progressLine.state = "running";

            loadTimeout.go(function() {
                if(progressLine.state === "running") {
                    connectionsPage.connections.abort();
                    progressLine.state = "idle";
                    errorMessage.value = i18n.tr("Loading following connections timed out");
                }
            });

            connectionsPage.connections.getNext(false, function(connection, state) {
                if(state === "SUCCESS") {
                    insertConnectionsRender(connection.getLastConnections());
                }
                else if(state !== "ABORT") {
                    errorMessage.value = i18n.tr("Loading following connections failed");
                }
                progressLine.state = "idle";
                updateConnectionDetail();
            });
        }
    }

    function prependConnections() {
        if(connectionsPage.connections) {
            progressLine.state = "running";

            loadTimeout.go(function() {
                if(progressLine.state === "running") {
                    connectionsPage.connections.abort();
                    progressLine.state = "idle";
                    errorMessage.value = i18n.tr("Loading previous connections timed out");
                }
            });

            connectionsPage.connections.getNext(true, function(connection, state) {
                if(state === "SUCCESS") {
                    var prevLength = connectionsModel.count;

                    renderAllConnections(connection);
                    connectionsView.forceLayout();

                    var newItemsCount = connection.connections.length - prevLength;

                    var topSize = 0;
                    var iterator = 0;
                    for(var child in connectionsView.contentItem.children) {
                        if(iterator >= newItemsCount) {
                            break;
                        }

                        topSize += connectionsView.contentItem.children[child].height;
                        iterator++;
                    }

                    scrollTo(topSize);
                }
                else if(state !== "ABORT") {
                    errorMessage.value = i18n.tr("Loading previous connections failed");
                }
                progressLine.state = "idle";
                updateConnectionDetail();
            });
        }
    }
    
    function scrollTo(top) {
        connectionsFlickable.contentY = top;
    }

    function renderAllConnections(connection) {
        if(connection) {
            connectionsModel.clearAll();
            insertConnectionsRender(connection.connections);
            updateConnectionDetail();
        }
    }

    function insertConnectionsRender(connections) {
        if(connections) {
            for(var i = 0; i < connections.length; i++) {
                connectionsModel.append({
                    distance: connections[i].getDistance(),
                    timeLength: connections[i].getTimeLength()
                });
                connectionsModel.childModel.push(connections[i]);
            }
        }
    }

    Component {
        id: infoBox

        Dialog {
            id: infoBoxDialogue
            title: i18n.tr("Connection overview info")
            text: connectionsPage.overviewText
            Button {
                text: i18n.tr("Ok")
                onClicked: PopupUtils.close(infoBoxDialogue)
            }
        }
    }

    CallbackTimer {
        id: loadTimeout
    }

    ConnectionsDelegate {
        id: connectionsDelegate
    }

    RoutesDelegate {
        id: routesDelegate
    }

    ProgressLine {
        id: progressLine
        anchors {
            top: pageHeader.bottom
        }
        z: 10
    }

    Item {
        id: pullInfo
        anchors {
            top: positionTop ? pageHeader.bottom : undefined
            left: parent.left
            right: parent.right
            bottom: !positionTop ? parent.bottom : undefined
            margins: units.gu(2)
        }
        height: pullInfoLabel.contentHeight
        z: 10
        visible: false
        
        property var text: ""
        property bool positionTop: true

        Label {
            id: pullInfoLabel
            anchors {
                centerIn: parent
                margins: units.gu(2)
            }
            text: parent.text
        }
    }
    
    ErrorMessage {
        id: errorMessage
        anchors.top: pageHeader.bottom
    }

    Flickable {
        id: connectionsFlickable
        anchors {
            top: errorMessage.bottom
            right: parent.right
            bottom: parent.bottom
            left: parent.left
        }
        contentWidth: width
        contentHeight: connectionsView.contentHeight
        flickableDirection: Flickable.VerticalFlick

        property int maxSensitivity: 150
        property var sensitivity: height / 10 > maxSensitivity ? maxSensitivity : height / 10
        property var inPull: false
        clip: true

        function checkDrag() {
            if(contentHeight > 0 && progressLine.state === "idle") {
                var topEdge = contentY + pageHeader.height;
                var bottomEdge = topEdge + height - pageHeader.height;

                if(bottomEdge > contentHeight + sensitivity) {
                    // Load more (next) connections;
                    return "append";
                }

                if(topEdge < -sensitivity) {
                    // Load more (previous) connections;
                    return "prepend";
                }
            }
            return false;
        }

        function drag(drag) {
            var drag = drag || connectionsFlickable.checkDrag();
            if(drag === "append") {
                connectionsPage.appendConnections();
                return true;
            }
            else if(drag === "prepend") {
                connectionsPage.prependConnections();
                return true;
            }
            return;
        }

        onContentYChanged: {
            if(connectionsFlickable.draggingVertically) {
                var drag = connectionsFlickable.checkDrag();
                if(drag) {
                    if(!dragTimer.running) {
                        if(!inPull) {
                            pullInfo.positionTop = drag === "prepend";
                            pullInfo.text = i18n.tr("Keep pulling to load more");
                            pullInfo.visible = true;
                        }
                        
                        dragTimer.go(function() {
                            var drag = connectionsFlickable.checkDrag();
                            if(drag) {
                                inPull = drag;
                                if(drag === "append") {
                                    pullInfo.positionTop = false;
                                    pullInfo.text = i18n.tr("Release to load following connections");
                                }
                                else if(drag === "prepend") {
                                    pullInfo.positionTop = true;
                                    pullInfo.text = i18n.tr("Release to load previous connections");
                                }
                                pullInfo.visible = true;
                            }
                            else {
                                inPull = false;
                            }
                        }, 500);
                    }
                }
                else {
                    pullInfo.visible = false;
                    inPull = false;
                }
            }
            else {
                pullInfo.visible = false;
                if(inPull) {
                    connectionsFlickable.drag(inPull);
                    inPull = false;
                }
            }
        }

        CallbackTimer {
            id: dragTimer
        }

        ListView {
            id: connectionsView
            anchors.fill: parent
            interactive: false
            delegate: connectionsDelegate

            model: ListModel {
                id: connectionsModel
                property var childModel: []

                function clearAll() {
                    this.clear();
                    this.childModel = [];
                }
            }
        }
    }

    Scrollbar {
        flickableItem: connectionsFlickable
        align: Qt.AlignTrailing
    }
}
